﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;

namespace Practica10
{
    public partial class frmVentana : Form
    {
        #region Atributos

        private static frmAcercaDe acercaDe;
        private static bool cambiosRealizados;

        #endregion

        public frmVentana()
        {
            InitializeComponent();
            setCambiosRealizados(false);
            cargarFuentesSistema();
        }

        #region Operaciones

        //Carga la lista de fuentes y gestiona el autocompletado de la selección de la fuente en el combo
        private void cargarFuentesSistema()
        {
            FontFamily[] ArrayFuentes = System.Drawing.FontFamily.Families;
            AutoCompleteStringCollection listaAutocompletado = new AutoCompleteStringCollection();

            foreach (System.Drawing.FontFamily fuente in ArrayFuentes)
            {
                cmbFuentes.Items.Add(fuente.Name);
                listaAutocompletado.Add(fuente.Name);
            }

            cmbFuentes.AutoCompleteCustomSource = listaAutocompletado;
            cmbFuentes.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbFuentes.AutoCompleteSource = AutoCompleteSource.CustomSource;

            cmbFuentes.SelectedItem = "Verdana";
            cmbTamanoFuente.SelectedItem = "10";
        }

        private void nuevoDocumento()
        {
            contenedorDocumento.Clear();
            setCambiosRealizados(false);
        }

        private void cargarFichero()
        {
            OpenFileDialog selectorFichero = new OpenFileDialog();
            selectorFichero.Title = "Apertura de fichero de texto enriquecido";
            selectorFichero.Filter = "Ficheros de texto (*.rtf)|*.rtf|Todos los ficheros (*.*)|*.*";
            selectorFichero.FilterIndex = 1;

            if (selectorFichero.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    contenedorDocumento.Clear();
                    contenedorDocumento.LoadFile(selectorFichero.FileName, RichTextBoxStreamType.RichText);
                    lblBarraEstadoDocumento.Text = "Documento actual: " + selectorFichero.FileName;
                    setCambiosRealizados(true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR en la operación de apertura del fichero: " + e);
                    MessageBox.Show("ERROR en la operación con el fichero.", "ERROR en la operación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void guardarFichero()
        {
            SaveFileDialog selectorFichero = new SaveFileDialog();
            selectorFichero.ValidateNames = true;
            selectorFichero.Title = "Guardado de fichero de texto enriquecido";
            selectorFichero.Filter = "Ficheros de texto (*.rtf)|*.rtf|Todos los ficheros (*.*)|*.*";
            selectorFichero.FilterIndex = 1;
            selectorFichero.RestoreDirectory = true;
            selectorFichero.OverwritePrompt = true;

            if (selectorFichero.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    contenedorDocumento.SaveFile(selectorFichero.FileName, RichTextBoxStreamType.RichText);
                    lblBarraEstadoDocumento.Text = "Documento actual: " + selectorFichero.FileName;
                    selectorFichero.Dispose();
                    setCambiosRealizados(false);
                }
                catch (Exception e)
                {
                    Console.WriteLine("ERROR en la operación de apertura de guardado del fichero: " + e);
                    MessageBox.Show("ERROR en la operación con el fichero.", "ERROR en la operación", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #region Formateado texto

        private void asignarNuevaFuente(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(contenedorDocumento.SelectedText))
            {
                Font fuenteAnterior = contenedorDocumento.SelectionFont;
                contenedorDocumento.SelectionFont = new Font(cmbFuentes.SelectedItem.ToString(), int.Parse(cmbTamanoFuente.SelectedItem.ToString()), fuenteAnterior.Style);
            }
        }

        private void asignarNegrita()
        {
            if (contenedorDocumento.SelectionFont != null)
            {
                FontStyle estiloActual = contenedorDocumento.SelectionFont.Style;

                if (contenedorDocumento.SelectionFont.Bold)
                {
                    estiloActual &= ~FontStyle.Bold;
                }
                else
                {
                    estiloActual |= FontStyle.Bold;

                }

                contenedorDocumento.SelectionFont = new Font(contenedorDocumento.SelectionFont, estiloActual);
            }
        }

        private void asignarCursiva()
        {
            if (contenedorDocumento.SelectionFont != null)
            {
                FontStyle estiloActual = contenedorDocumento.SelectionFont.Style;

                if (contenedorDocumento.SelectionFont.Italic)
                {
                    estiloActual &= ~FontStyle.Italic;
                }
                else
                {
                    estiloActual |= FontStyle.Italic;

                }

                contenedorDocumento.SelectionFont = new Font(contenedorDocumento.SelectionFont, estiloActual);
            }
        }

        private void asignarSubrayado()
        {
            if (contenedorDocumento.SelectionFont != null)
            {
                FontStyle estiloActual = contenedorDocumento.SelectionFont.Style;

                if (contenedorDocumento.SelectionFont.Underline)
                {
                    estiloActual &= ~FontStyle.Underline;
                }
                else
                {
                    estiloActual |= FontStyle.Underline;

                }

                contenedorDocumento.SelectionFont = new Font(contenedorDocumento.SelectionFont, estiloActual);
            }
        }

        private void cambiarColor()
        {
            ColorDialog colorActual = new ColorDialog();

            colorActual.Color = contenedorDocumento.SelectionColor;

            if (colorActual.ShowDialog() == DialogResult.OK &&
               colorActual.Color != contenedorDocumento.SelectionColor)
            {
                contenedorDocumento.SelectionColor = colorActual.Color;
            }
        }

        private void cambiarColorFondo()
        {
            ColorDialog colorActual = new ColorDialog();

            colorActual.Color = contenedorDocumento.SelectionBackColor;

            if (colorActual.ShowDialog() == DialogResult.OK &&
               colorActual.Color != contenedorDocumento.SelectionBackColor)
            {
                contenedorDocumento.SelectionBackColor = colorActual.Color;
            }
        }

        private void cambiarAlineacionIzq(object sender, EventArgs e)
        {
            contenedorDocumento.SelectionAlignment = HorizontalAlignment.Left;
        }

        private void cambiarAlineacionCentro(object sender, EventArgs e)
        {
            contenedorDocumento.SelectionAlignment = HorizontalAlignment.Center;
        }

        private void cambiarAlineacionDer(object sender, EventArgs e)
        {
            contenedorDocumento.SelectionAlignment = HorizontalAlignment.Right;
        }

        #endregion

        private void buscarEnDoc()
        {
            contenedorDocumento.Find(txtBuscar.Text, RichTextBoxFinds.None);
            txtBuscar.Text = "Buscar...";
        }

        private void setCambiosRealizados(bool estado)
        {
            if (estado)
            {
                lblEstadoCambios.Text = "Hay cambios pendientes de guardar";
                lblEstadoCambios.Image = ((System.Drawing.Image)(Properties.Resources.guardarNo));
            }
            else
            {
                lblEstadoCambios.Text = "Sin cambios";
                lblEstadoCambios.Image = ((System.Drawing.Image)(Properties.Resources.guardar));
                menuBarraBotonRehacer.Enabled = false;
            }
            cambiosRealizados = estado;
            menuBarraBtnGuardar.Enabled = estado;
            menuHerramientasGuardar.Enabled = estado;
            menuHerramientasDeshacer.Enabled = estado;
            menuHerramientasRehacer.Enabled = estado;
            menuBarraBotonDeshacer.Enabled = estado;
        }

        #endregion

        #region Menús y eventos

        private void menuAcercaDe_Click(object sender, EventArgs e)
        {
            acercaDe = new frmAcercaDe();
            acercaDe.ShowDialog();
        }

        private void frmVentana_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (cambiosRealizados)
                {
                    DialogResult respuesta = MessageBox.Show("¿Desea guardar los cambios realizados ahora?\n\nSi elige NO se continuará con el cierre perdiendo los cambios.", "Elementos sin guardar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                    if (respuesta == DialogResult.Yes)
                    {
                        guardarFichero();
                    }
                    else if (respuesta == DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    DialogResult dialogo = MessageBox.Show("¿Desea Salir de la Aplicacion?", "Salir de Aplicacion", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                    if (dialogo != DialogResult.OK)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void menuBarraBtnAbrirGrupo_Click(object sender, EventArgs e)
        {
            if (cambiosRealizados)
            {
                DialogResult respuesta = MessageBox.Show("Hay cambios realizados sin guardar. ¿Desea guardar los cambios realizados ahora?\n\nSi elige NO se continuará con el cierre perdiendo los cambios.", "Elementos sin guardar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (respuesta == DialogResult.No)
                {
                    cargarFichero();
                }
                else if (respuesta == DialogResult.Yes)
                {
                    guardarFichero();
                }
            }
            else
            {
                cargarFichero();
            }
            
        }

        private void menuBarraBtnGuardar_Click(object sender, EventArgs e)
        {
            if(cambiosRealizados)
                guardarFichero();
        }

        private void menuBarraBtnNegrita_Click(object sender, EventArgs e)
        {
            asignarNegrita();
        }

        private void menuBarraBtnCursiva_Click(object sender, EventArgs e)
        {
            asignarCursiva();
        }

        private void menuBarraBtnSubrayado_Click(object sender, EventArgs e)
        {
            asignarSubrayado();
        }

        private void menuBarraBtnBuscar_Click(object sender, EventArgs e)
        {
            buscarEnDoc();
        }

        private void deteccionFormatoPortapapeles()
        {
            if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) ||
                Clipboard.GetDataObject().GetDataPresent(DataFormats.Rtf))
            {
                contenedorDocumento.Paste();
                setCambiosRealizados(false);
            }
            else
            {
                MessageBox.Show("El formato del contenido del portapapeles no es aceptado por la aplicación.", "Formato inválido portapapeles", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void deteccionCambioTexto(object sender, EventArgs e)
        {
            setCambiosRealizados(true);
        }

        private void vaciarTxtBuscar(object sender, EventArgs e)
        {
            txtBuscar.Text = "";
        }

        private void cambiarColorFuente(object sender, EventArgs e)
        {
            cambiarColor();
        }

        private void menuBarraBotonFondo_Click(object sender, EventArgs e)
        {
            cambiarColorFondo();
        }

        private void seleccionartodoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contenedorDocumento.SelectAll();
        }

        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contenedorDocumento.Copy();
        }

        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deteccionFormatoPortapapeles();
        }

        private void cortarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contenedorDocumento.Cut();
        }

        private void deshacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contenedorDocumento.CanUndo)
            {
                contenedorDocumento.Undo();
                menuBarraBotonRehacer.Enabled = true;
            }
        }

        private void rehacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (contenedorDocumento.CanRedo)
            {
                contenedorDocumento.Redo();
            }
        }

        private void menuNotificacionSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mostrarVentana(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void menuBarraBtnNuevoGrupo_Click(object sender, EventArgs e)
        {
            if (cambiosRealizados)
            {
                DialogResult respuesta = MessageBox.Show("Hay cambios realizados sin guardar. ¿Desea guardar los cambios realizados ahora?\n\nSi elige NO se continuará con el cierre perdiendo los cambios.", "Elementos sin guardar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (respuesta == DialogResult.No)
                {
                    nuevoDocumento();
                }
                else if (respuesta == DialogResult.Yes)
                {
                    guardarFichero();
                }
            }
        }

        #endregion

    }
}
