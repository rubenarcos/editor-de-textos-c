﻿namespace Practica10
{
    partial class frmVentana
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVentana));
            this.contenedorDocumento = new System.Windows.Forms.RichTextBox();
            this.barraEstado = new System.Windows.Forms.StatusStrip();
            this.lblBarraEstadoDocumento = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblEstadoCambios = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuBarraHerramientas = new System.Windows.Forms.ToolStrip();
            this.menuBarraBtnNuevoGrupo = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBtnAbrirGrupo = new System.Windows.Forms.ToolStripButton();
            this.separadorNuevo = new System.Windows.Forms.ToolStripSeparator();
            this.menuBarraBtnGuardar = new System.Windows.Forms.ToolStripButton();
            this.separadorGuardar = new System.Windows.Forms.ToolStripSeparator();
            this.menuBarraBotonDeshacer = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBotonRehacer = new System.Windows.Forms.ToolStripButton();
            this.separadorFormato2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuBarraBtnNegrita = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBtnCursiva = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBtnSubrayado = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBtnColor = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBotonFondo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.menuBarraBotonAlineacionCentro = new System.Windows.Forms.ToolStripButton();
            this.menuBotonBarraAlineacionDer = new System.Windows.Forms.ToolStripButton();
            this.separadorFormato = new System.Windows.Forms.ToolStripSeparator();
            this.cmbFuentes = new System.Windows.Forms.ToolStripComboBox();
            this.cmbTamanoFuente = new System.Windows.Forms.ToolStripComboBox();
            this.separadorBusqueda = new System.Windows.Forms.ToolStripSeparator();
            this.txtBuscar = new System.Windows.Forms.ToolStripTextBox();
            this.menuBarraBtnBuscar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuOpciones = new System.Windows.Forms.MenuStrip();
            this.menuHerramientasArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasNuevo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasGuardar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHerramientasSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasEditar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasDeshacer = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasRehacer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHerramientasCortar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasCopiar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasPegar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuHerramientasSeleccionarTodo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasAyuda = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientasAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.iconoNotificacion = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuIconoNotificacion = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuNotificacionNuevo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNotificacionAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNotificacionAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.separadorMenuNotificacionSalir = new System.Windows.Forms.ToolStripSeparator();
            this.menuNotificacionSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.barraEstado.SuspendLayout();
            this.menuBarraHerramientas.SuspendLayout();
            this.menuOpciones.SuspendLayout();
            this.menuIconoNotificacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // contenedorDocumento
            // 
            this.contenedorDocumento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contenedorDocumento.Location = new System.Drawing.Point(0, 49);
            this.contenedorDocumento.Margin = new System.Windows.Forms.Padding(0);
            this.contenedorDocumento.Name = "contenedorDocumento";
            this.contenedorDocumento.Size = new System.Drawing.Size(684, 370);
            this.contenedorDocumento.TabIndex = 1;
            this.contenedorDocumento.Text = "";
            this.contenedorDocumento.TextChanged += new System.EventHandler(this.deteccionCambioTexto);
            // 
            // barraEstado
            // 
            this.barraEstado.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblBarraEstadoDocumento,
            this.lblEstadoCambios});
            this.barraEstado.Location = new System.Drawing.Point(0, 419);
            this.barraEstado.Name = "barraEstado";
            this.barraEstado.Size = new System.Drawing.Size(684, 22);
            this.barraEstado.TabIndex = 2;
            this.barraEstado.Text = "statusStrip1";
            // 
            // lblBarraEstadoDocumento
            // 
            this.lblBarraEstadoDocumento.Name = "lblBarraEstadoDocumento";
            this.lblBarraEstadoDocumento.Size = new System.Drawing.Size(119, 17);
            this.lblBarraEstadoDocumento.Text = "Documento sin título";
            // 
            // lblEstadoCambios
            // 
            this.lblEstadoCambios.Image = global::Practica10.Properties.Resources.guardar;
            this.lblEstadoCambios.Margin = new System.Windows.Forms.Padding(10, 3, 10, 2);
            this.lblEstadoCambios.Name = "lblEstadoCambios";
            this.lblEstadoCambios.Size = new System.Drawing.Size(87, 17);
            this.lblEstadoCambios.Text = "Sin cambios";
            this.lblEstadoCambios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menuBarraHerramientas
            // 
            this.menuBarraHerramientas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBarraBtnNuevoGrupo,
            this.menuBarraBtnAbrirGrupo,
            this.separadorNuevo,
            this.menuBarraBtnGuardar,
            this.separadorGuardar,
            this.menuBarraBotonDeshacer,
            this.menuBarraBotonRehacer,
            this.separadorFormato2,
            this.menuBarraBtnNegrita,
            this.menuBarraBtnCursiva,
            this.menuBarraBtnSubrayado,
            this.menuBarraBtnColor,
            this.menuBarraBotonFondo,
            this.toolStripSeparator8,
            this.toolStripButton1,
            this.menuBarraBotonAlineacionCentro,
            this.menuBotonBarraAlineacionDer,
            this.separadorFormato,
            this.cmbFuentes,
            this.cmbTamanoFuente,
            this.separadorBusqueda,
            this.txtBuscar,
            this.menuBarraBtnBuscar,
            this.toolStripSeparator6});
            this.menuBarraHerramientas.Location = new System.Drawing.Point(0, 24);
            this.menuBarraHerramientas.Name = "menuBarraHerramientas";
            this.menuBarraHerramientas.Size = new System.Drawing.Size(684, 25);
            this.menuBarraHerramientas.TabIndex = 3;
            this.menuBarraHerramientas.Text = "toolStrip1";
            // 
            // menuBarraBtnNuevoGrupo
            // 
            this.menuBarraBtnNuevoGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuBarraBtnNuevoGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnNuevoGrupo.Image = global::Practica10.Properties.Resources.nuevoGrupo;
            this.menuBarraBtnNuevoGrupo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnNuevoGrupo.Name = "menuBarraBtnNuevoGrupo";
            this.menuBarraBtnNuevoGrupo.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnNuevoGrupo.Text = "&Nuevo (F3)";
            this.menuBarraBtnNuevoGrupo.Click += new System.EventHandler(this.menuBarraBtnNuevoGrupo_Click);
            // 
            // menuBarraBtnAbrirGrupo
            // 
            this.menuBarraBtnAbrirGrupo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuBarraBtnAbrirGrupo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnAbrirGrupo.Image = global::Practica10.Properties.Resources.abrirGrupo;
            this.menuBarraBtnAbrirGrupo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnAbrirGrupo.Name = "menuBarraBtnAbrirGrupo";
            this.menuBarraBtnAbrirGrupo.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnAbrirGrupo.Text = "&Abrir Grupo (F4)";
            this.menuBarraBtnAbrirGrupo.Click += new System.EventHandler(this.menuBarraBtnAbrirGrupo_Click);
            // 
            // separadorNuevo
            // 
            this.separadorNuevo.Name = "separadorNuevo";
            this.separadorNuevo.Size = new System.Drawing.Size(6, 25);
            // 
            // menuBarraBtnGuardar
            // 
            this.menuBarraBtnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuBarraBtnGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnGuardar.Enabled = false;
            this.menuBarraBtnGuardar.Image = global::Practica10.Properties.Resources.guardar;
            this.menuBarraBtnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnGuardar.Name = "menuBarraBtnGuardar";
            this.menuBarraBtnGuardar.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnGuardar.Text = "&Guardar (F5)";
            this.menuBarraBtnGuardar.Click += new System.EventHandler(this.menuBarraBtnGuardar_Click);
            // 
            // separadorGuardar
            // 
            this.separadorGuardar.Name = "separadorGuardar";
            this.separadorGuardar.Size = new System.Drawing.Size(6, 25);
            // 
            // menuBarraBotonDeshacer
            // 
            this.menuBarraBotonDeshacer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBotonDeshacer.Enabled = false;
            this.menuBarraBotonDeshacer.Image = global::Practica10.Properties.Resources.deshacer;
            this.menuBarraBotonDeshacer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBotonDeshacer.Name = "menuBarraBotonDeshacer";
            this.menuBarraBotonDeshacer.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBotonDeshacer.Text = "&Deshacer (Ctrl+Z)";
            this.menuBarraBotonDeshacer.Click += new System.EventHandler(this.deshacerToolStripMenuItem_Click);
            // 
            // menuBarraBotonRehacer
            // 
            this.menuBarraBotonRehacer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBotonRehacer.Enabled = false;
            this.menuBarraBotonRehacer.Image = global::Practica10.Properties.Resources.rehacer;
            this.menuBarraBotonRehacer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBotonRehacer.Name = "menuBarraBotonRehacer";
            this.menuBarraBotonRehacer.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBotonRehacer.Text = "&Rehacer (Ctrl+Y)";
            this.menuBarraBotonRehacer.Click += new System.EventHandler(this.rehacerToolStripMenuItem_Click);
            // 
            // separadorFormato2
            // 
            this.separadorFormato2.Name = "separadorFormato2";
            this.separadorFormato2.Size = new System.Drawing.Size(6, 25);
            // 
            // menuBarraBtnNegrita
            // 
            this.menuBarraBtnNegrita.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnNegrita.Image = global::Practica10.Properties.Resources.negrita;
            this.menuBarraBtnNegrita.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnNegrita.Name = "menuBarraBtnNegrita";
            this.menuBarraBtnNegrita.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnNegrita.Text = "&Negrita (Ctrl+N)";
            this.menuBarraBtnNegrita.Click += new System.EventHandler(this.menuBarraBtnNegrita_Click);
            // 
            // menuBarraBtnCursiva
            // 
            this.menuBarraBtnCursiva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnCursiva.Image = global::Practica10.Properties.Resources.cursiva;
            this.menuBarraBtnCursiva.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnCursiva.Name = "menuBarraBtnCursiva";
            this.menuBarraBtnCursiva.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnCursiva.Text = "&Cursiva (Ctrl+K)";
            this.menuBarraBtnCursiva.Click += new System.EventHandler(this.menuBarraBtnCursiva_Click);
            // 
            // menuBarraBtnSubrayado
            // 
            this.menuBarraBtnSubrayado.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnSubrayado.Image = global::Practica10.Properties.Resources.subrayado;
            this.menuBarraBtnSubrayado.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnSubrayado.Name = "menuBarraBtnSubrayado";
            this.menuBarraBtnSubrayado.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnSubrayado.Text = "&Subrayado (Ctrl+S)";
            this.menuBarraBtnSubrayado.Click += new System.EventHandler(this.menuBarraBtnSubrayado_Click);
            // 
            // menuBarraBtnColor
            // 
            this.menuBarraBtnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnColor.Image = global::Practica10.Properties.Resources.colorFuente;
            this.menuBarraBtnColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnColor.Name = "menuBarraBtnColor";
            this.menuBarraBtnColor.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnColor.Text = "&Color fuente";
            this.menuBarraBtnColor.Click += new System.EventHandler(this.cambiarColorFuente);
            // 
            // menuBarraBotonFondo
            // 
            this.menuBarraBotonFondo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBotonFondo.Image = global::Practica10.Properties.Resources.colorFondoFuente;
            this.menuBarraBotonFondo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBotonFondo.Name = "menuBarraBotonFondo";
            this.menuBarraBotonFondo.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBotonFondo.Text = "Color &fondo";
            this.menuBarraBotonFondo.Click += new System.EventHandler(this.menuBarraBotonFondo_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Practica10.Properties.Resources.alineacionIzq;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Alineación Izquierda";
            this.toolStripButton1.Click += new System.EventHandler(this.cambiarAlineacionIzq);
            // 
            // menuBarraBotonAlineacionCentro
            // 
            this.menuBarraBotonAlineacionCentro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBotonAlineacionCentro.Image = global::Practica10.Properties.Resources.alineacionCentro;
            this.menuBarraBotonAlineacionCentro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBotonAlineacionCentro.Name = "menuBarraBotonAlineacionCentro";
            this.menuBarraBotonAlineacionCentro.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBotonAlineacionCentro.Text = "Alineación Centrada";
            this.menuBarraBotonAlineacionCentro.Click += new System.EventHandler(this.cambiarAlineacionCentro);
            // 
            // menuBotonBarraAlineacionDer
            // 
            this.menuBotonBarraAlineacionDer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBotonBarraAlineacionDer.Image = global::Practica10.Properties.Resources.alineacionDer;
            this.menuBotonBarraAlineacionDer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBotonBarraAlineacionDer.Name = "menuBotonBarraAlineacionDer";
            this.menuBotonBarraAlineacionDer.Size = new System.Drawing.Size(23, 22);
            this.menuBotonBarraAlineacionDer.Text = "Alineación Derecha";
            this.menuBotonBarraAlineacionDer.Click += new System.EventHandler(this.cambiarAlineacionDer);
            // 
            // separadorFormato
            // 
            this.separadorFormato.Name = "separadorFormato";
            this.separadorFormato.Size = new System.Drawing.Size(6, 25);
            // 
            // cmbFuentes
            // 
            this.cmbFuentes.DropDownWidth = 200;
            this.cmbFuentes.Name = "cmbFuentes";
            this.cmbFuentes.Size = new System.Drawing.Size(121, 25);
            this.cmbFuentes.SelectedIndexChanged += new System.EventHandler(this.asignarNuevaFuente);
            // 
            // cmbTamanoFuente
            // 
            this.cmbTamanoFuente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTamanoFuente.DropDownWidth = 50;
            this.cmbTamanoFuente.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "14",
            "16",
            "18",
            "20",
            "22",
            "24",
            "26",
            "28",
            "32",
            "48",
            "72"});
            this.cmbTamanoFuente.Name = "cmbTamanoFuente";
            this.cmbTamanoFuente.Size = new System.Drawing.Size(75, 25);
            this.cmbTamanoFuente.SelectedIndexChanged += new System.EventHandler(this.asignarNuevaFuente);
            // 
            // separadorBusqueda
            // 
            this.separadorBusqueda.Name = "separadorBusqueda";
            this.separadorBusqueda.Size = new System.Drawing.Size(6, 25);
            // 
            // txtBuscar
            // 
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(100, 25);
            this.txtBuscar.Text = "Buscar...";
            this.txtBuscar.Enter += new System.EventHandler(this.vaciarTxtBuscar);
            // 
            // menuBarraBtnBuscar
            // 
            this.menuBarraBtnBuscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.menuBarraBtnBuscar.Image = global::Practica10.Properties.Resources.buscar;
            this.menuBarraBtnBuscar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuBarraBtnBuscar.Name = "menuBarraBtnBuscar";
            this.menuBarraBtnBuscar.Size = new System.Drawing.Size(23, 22);
            this.menuBarraBtnBuscar.Text = "&Buscar";
            this.menuBarraBtnBuscar.ToolTipText = "Buscar un texto en el documento";
            this.menuBarraBtnBuscar.Click += new System.EventHandler(this.menuBarraBtnBuscar_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // menuOpciones
            // 
            this.menuOpciones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHerramientasArchivo,
            this.menuHerramientasEditar,
            this.menuHerramientasAyuda});
            this.menuOpciones.Location = new System.Drawing.Point(0, 0);
            this.menuOpciones.Name = "menuOpciones";
            this.menuOpciones.Size = new System.Drawing.Size(684, 24);
            this.menuOpciones.TabIndex = 4;
            // 
            // menuHerramientasArchivo
            // 
            this.menuHerramientasArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHerramientasNuevo,
            this.menuHerramientasAbrir,
            this.menuHerramientasGuardar,
            this.toolStripSeparator1,
            this.menuHerramientasSalir});
            this.menuHerramientasArchivo.Name = "menuHerramientasArchivo";
            this.menuHerramientasArchivo.Size = new System.Drawing.Size(60, 20);
            this.menuHerramientasArchivo.Text = "&Archivo";
            // 
            // menuHerramientasNuevo
            // 
            this.menuHerramientasNuevo.Image = ((System.Drawing.Image)(resources.GetObject("menuHerramientasNuevo.Image")));
            this.menuHerramientasNuevo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuHerramientasNuevo.Name = "menuHerramientasNuevo";
            this.menuHerramientasNuevo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuHerramientasNuevo.Size = new System.Drawing.Size(158, 22);
            this.menuHerramientasNuevo.Text = "&Nuevo";
            this.menuHerramientasNuevo.Click += new System.EventHandler(this.menuBarraBtnNuevoGrupo_Click);
            // 
            // menuHerramientasAbrir
            // 
            this.menuHerramientasAbrir.Image = ((System.Drawing.Image)(resources.GetObject("menuHerramientasAbrir.Image")));
            this.menuHerramientasAbrir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuHerramientasAbrir.Name = "menuHerramientasAbrir";
            this.menuHerramientasAbrir.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menuHerramientasAbrir.Size = new System.Drawing.Size(161, 22);
            this.menuHerramientasAbrir.Text = "&Abrir";
            this.menuHerramientasAbrir.Click += new System.EventHandler(this.menuBarraBtnAbrirGrupo_Click);
            // 
            // menuHerramientasGuardar
            // 
            this.menuHerramientasGuardar.Enabled = false;
            this.menuHerramientasGuardar.Image = ((System.Drawing.Image)(resources.GetObject("menuHerramientasGuardar.Image")));
            this.menuHerramientasGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuHerramientasGuardar.Name = "menuHerramientasGuardar";
            this.menuHerramientasGuardar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.menuHerramientasGuardar.Size = new System.Drawing.Size(161, 22);
            this.menuHerramientasGuardar.Text = "&Guardar";
            this.menuHerramientasGuardar.Click += new System.EventHandler(this.menuBarraBtnGuardar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(158, 6);
            // 
            // menuHerramientasSalir
            // 
            this.menuHerramientasSalir.Name = "menuHerramientasSalir";
            this.menuHerramientasSalir.Size = new System.Drawing.Size(161, 22);
            this.menuHerramientasSalir.Text = "&Salir";
            this.menuHerramientasSalir.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // menuHerramientasEditar
            // 
            this.menuHerramientasEditar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHerramientasDeshacer,
            this.menuHerramientasRehacer,
            this.toolStripSeparator3,
            this.menuHerramientasCortar,
            this.menuHerramientasCopiar,
            this.menuHerramientasPegar,
            this.toolStripSeparator4,
            this.menuHerramientasSeleccionarTodo});
            this.menuHerramientasEditar.Name = "menuHerramientasEditar";
            this.menuHerramientasEditar.Size = new System.Drawing.Size(49, 20);
            this.menuHerramientasEditar.Text = "&Editar";
            // 
            // menuHerramientasDeshacer
            // 
            this.menuHerramientasDeshacer.Enabled = false;
            this.menuHerramientasDeshacer.Image = global::Practica10.Properties.Resources.deshacer;
            this.menuHerramientasDeshacer.Name = "menuHerramientasDeshacer";
            this.menuHerramientasDeshacer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.menuHerramientasDeshacer.Size = new System.Drawing.Size(204, 22);
            this.menuHerramientasDeshacer.Text = "&Deshacer";
            this.menuHerramientasDeshacer.Click += new System.EventHandler(this.deshacerToolStripMenuItem_Click);
            // 
            // menuHerramientasRehacer
            // 
            this.menuHerramientasRehacer.Enabled = false;
            this.menuHerramientasRehacer.Image = global::Practica10.Properties.Resources.rehacer;
            this.menuHerramientasRehacer.Name = "menuHerramientasRehacer";
            this.menuHerramientasRehacer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.menuHerramientasRehacer.Size = new System.Drawing.Size(204, 22);
            this.menuHerramientasRehacer.Text = "&Rehacer";
            this.menuHerramientasRehacer.Click += new System.EventHandler(this.rehacerToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(201, 6);
            // 
            // menuHerramientasCortar
            // 
            this.menuHerramientasCortar.Image = ((System.Drawing.Image)(resources.GetObject("menuHerramientasCortar.Image")));
            this.menuHerramientasCortar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuHerramientasCortar.Name = "menuHerramientasCortar";
            this.menuHerramientasCortar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.menuHerramientasCortar.Size = new System.Drawing.Size(204, 22);
            this.menuHerramientasCortar.Text = "Cor&tar";
            this.menuHerramientasCortar.Click += new System.EventHandler(this.cortarToolStripMenuItem_Click);
            // 
            // menuHerramientasCopiar
            // 
            this.menuHerramientasCopiar.Image = ((System.Drawing.Image)(resources.GetObject("menuHerramientasCopiar.Image")));
            this.menuHerramientasCopiar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuHerramientasCopiar.Name = "menuHerramientasCopiar";
            this.menuHerramientasCopiar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.menuHerramientasCopiar.Size = new System.Drawing.Size(204, 22);
            this.menuHerramientasCopiar.Text = "&Copiar";
            this.menuHerramientasCopiar.Click += new System.EventHandler(this.copiarToolStripMenuItem_Click);
            // 
            // menuHerramientasPegar
            // 
            this.menuHerramientasPegar.Image = ((System.Drawing.Image)(resources.GetObject("menuHerramientasPegar.Image")));
            this.menuHerramientasPegar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.menuHerramientasPegar.Name = "menuHerramientasPegar";
            this.menuHerramientasPegar.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.menuHerramientasPegar.Size = new System.Drawing.Size(204, 22);
            this.menuHerramientasPegar.Text = "&Pegar";
            this.menuHerramientasPegar.Click += new System.EventHandler(this.pegarToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(201, 6);
            // 
            // menuHerramientasSeleccionarTodo
            // 
            this.menuHerramientasSeleccionarTodo.Name = "menuHerramientasSeleccionarTodo";
            this.menuHerramientasSeleccionarTodo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.menuHerramientasSeleccionarTodo.Size = new System.Drawing.Size(204, 22);
            this.menuHerramientasSeleccionarTodo.Text = "&Seleccionar todo";
            this.menuHerramientasSeleccionarTodo.Click += new System.EventHandler(this.seleccionartodoToolStripMenuItem_Click);
            // 
            // menuHerramientasAyuda
            // 
            this.menuHerramientasAyuda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuHerramientasAcercaDe});
            this.menuHerramientasAyuda.Name = "menuHerramientasAyuda";
            this.menuHerramientasAyuda.Size = new System.Drawing.Size(53, 20);
            this.menuHerramientasAyuda.Text = "Ay&uda";
            // 
            // menuHerramientasAcercaDe
            // 
            this.menuHerramientasAcercaDe.Name = "menuHerramientasAcercaDe";
            this.menuHerramientasAcercaDe.Size = new System.Drawing.Size(135, 22);
            this.menuHerramientasAcercaDe.Text = "&Acerca de...";
            this.menuHerramientasAcercaDe.Click += new System.EventHandler(this.menuAcercaDe_Click);
            // 
            // iconoNotificacion
            // 
            this.iconoNotificacion.BalloonTipText = "Título prueba";
            this.iconoNotificacion.ContextMenuStrip = this.menuIconoNotificacion;
            this.iconoNotificacion.Icon = ((System.Drawing.Icon)(resources.GetObject("iconoNotificacion.Icon")));
            this.iconoNotificacion.Text = "Práctica 10 - Rubén Arcos";
            this.iconoNotificacion.Visible = true;
            this.iconoNotificacion.DoubleClick += new System.EventHandler(this.mostrarVentana);
            // 
            // menuIconoNotificacion
            // 
            this.menuIconoNotificacion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNotificacionNuevo,
            this.menuNotificacionAbrir,
            this.menuNotificacionAcercaDe,
            this.separadorMenuNotificacionSalir,
            this.menuNotificacionSalir});
            this.menuIconoNotificacion.Name = "menuIconoNotificacion";
            this.menuIconoNotificacion.Size = new System.Drawing.Size(127, 98);
            // 
            // menuNotificacionNuevo
            // 
            this.menuNotificacionNuevo.Name = "menuNotificacionNuevo";
            this.menuNotificacionNuevo.Size = new System.Drawing.Size(126, 22);
            this.menuNotificacionNuevo.Text = "&Nuevo";
            this.menuNotificacionNuevo.Click += new System.EventHandler(this.menuBarraBtnNuevoGrupo_Click);
            // 
            // menuNotificacionAbrir
            // 
            this.menuNotificacionAbrir.Name = "menuNotificacionAbrir";
            this.menuNotificacionAbrir.Size = new System.Drawing.Size(126, 22);
            this.menuNotificacionAbrir.Text = "&Abrir";
            this.menuNotificacionAbrir.Click += new System.EventHandler(this.menuBarraBtnAbrirGrupo_Click);
            // 
            // menuNotificacionAcercaDe
            // 
            this.menuNotificacionAcercaDe.Name = "menuNotificacionAcercaDe";
            this.menuNotificacionAcercaDe.Size = new System.Drawing.Size(126, 22);
            this.menuNotificacionAcercaDe.Text = "&Acerca de";
            this.menuNotificacionAcercaDe.Click += new System.EventHandler(this.menuAcercaDe_Click);
            // 
            // separadorMenuNotificacionSalir
            // 
            this.separadorMenuNotificacionSalir.Name = "separadorMenuNotificacionSalir";
            this.separadorMenuNotificacionSalir.Size = new System.Drawing.Size(123, 6);
            // 
            // menuNotificacionSalir
            // 
            this.menuNotificacionSalir.Name = "menuNotificacionSalir";
            this.menuNotificacionSalir.Size = new System.Drawing.Size(126, 22);
            this.menuNotificacionSalir.Text = "&Salir";
            this.menuNotificacionSalir.Click += new System.EventHandler(this.menuNotificacionSalir_Click);
            // 
            // frmVentana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 441);
            this.Controls.Add(this.menuBarraHerramientas);
            this.Controls.Add(this.menuOpciones);
            this.Controls.Add(this.barraEstado);
            this.Controls.Add(this.contenedorDocumento);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVentana";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Práctica 10";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVentana_FormClosing);
            this.barraEstado.ResumeLayout(false);
            this.barraEstado.PerformLayout();
            this.menuBarraHerramientas.ResumeLayout(false);
            this.menuBarraHerramientas.PerformLayout();
            this.menuOpciones.ResumeLayout(false);
            this.menuOpciones.PerformLayout();
            this.menuIconoNotificacion.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RichTextBox contenedorDocumento;
        private System.Windows.Forms.StatusStrip barraEstado;
        private System.Windows.Forms.ToolStrip menuBarraHerramientas;
        private System.Windows.Forms.ToolStripButton menuBarraBtnNuevoGrupo;
        private System.Windows.Forms.ToolStripButton menuBarraBtnAbrirGrupo;
        private System.Windows.Forms.ToolStripSeparator separadorNuevo;
        private System.Windows.Forms.ToolStripButton menuBarraBtnGuardar;
        private System.Windows.Forms.MenuStrip menuOpciones;
        private System.Windows.Forms.ToolStripButton menuBarraBtnNegrita;
        private System.Windows.Forms.ToolStripButton menuBarraBtnCursiva;
        private System.Windows.Forms.ToolStripButton menuBarraBtnSubrayado;
        private System.Windows.Forms.ToolStripSeparator separadorGuardar;
        private System.Windows.Forms.ToolStripButton menuBarraBtnBuscar;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasArchivo;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasNuevo;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasAbrir;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasGuardar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasSalir;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasEditar;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasDeshacer;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasRehacer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasCortar;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasCopiar;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasPegar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasSeleccionarTodo;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasAyuda;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientasAcercaDe;
        private System.Windows.Forms.ToolStripTextBox txtBuscar;
        private System.Windows.Forms.ToolStripButton menuBarraBtnColor;
        private System.Windows.Forms.ToolStripStatusLabel lblBarraEstadoDocumento;
        private System.Windows.Forms.ToolStripStatusLabel lblEstadoCambios;
        private System.Windows.Forms.NotifyIcon iconoNotificacion;
        private System.Windows.Forms.ContextMenuStrip menuIconoNotificacion;
        private System.Windows.Forms.ToolStripMenuItem menuNotificacionSalir;
        private System.Windows.Forms.ToolStripMenuItem menuNotificacionAbrir;
        private System.Windows.Forms.ToolStripMenuItem menuNotificacionAcercaDe;
        private System.Windows.Forms.ToolStripSeparator separadorMenuNotificacionSalir;
        private System.Windows.Forms.ToolStripMenuItem menuNotificacionNuevo;
        private System.Windows.Forms.ToolStripButton menuBarraBotonFondo;
        private System.Windows.Forms.ToolStripButton menuBarraBotonDeshacer;
        private System.Windows.Forms.ToolStripButton menuBarraBotonRehacer;
        private System.Windows.Forms.ToolStripSeparator separadorFormato2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton menuBarraBotonAlineacionCentro;
        private System.Windows.Forms.ToolStripButton menuBotonBarraAlineacionDer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator separadorBusqueda;
        private System.Windows.Forms.ToolStripSeparator separadorFormato;
        private System.Windows.Forms.ToolStripComboBox cmbFuentes;
        private System.Windows.Forms.ToolStripComboBox cmbTamanoFuente;
    }
}

